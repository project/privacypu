<?php

/**
 * @file
 *
 * Provide a Views plugin for privacypu module.
 */

/**
 * Implements hook_views_plugins().
 */
function privacypu_views_plugins() {
  return array(
    'module' => 'views', // This just tells our themes are elsewhere.
    'argument validator' => array(
      'privacypu' => array(
        'title' => t('Privacy Per User'),
        'handler' => 'privacypu_plugin_argument_validate_privacypu',
        'path' => drupal_get_path('module', 'privacypu'),
      ),
    ),
  );
}