<?php

/**
 * @file
 * Admin settings for Privacy Per User.
 */

/**
 * Admin settings form.
 */
function privacypu_admin_settings() {
  $form = array();
  $options = privacypu_get_state_options();

  $form['privacypu_defaults'] = array(
    '#type' => 'fieldset',
    '#title' => t('General settings'),
  );

  $form['privacypu_defaults']['privacypu_default_state'] = array(
    '#type' => 'select',
    '#title' => t('Default privacy state'),
    '#options' => $options,
    '#default_value' => variable_get('privacypu_default_state', 'private'),
  );

  return system_settings_form($form);
}