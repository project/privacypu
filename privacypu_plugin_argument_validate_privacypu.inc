<?php

/**
 * @file
 * Contains the privacypu argument validator plugin.
 */

/**
 * Provide privacypu access check to validate whether or not an argument is ok.
 *
 * @ingroup views_argument_validate_plugins
 */
class privacypu_plugin_argument_validate_privacypu extends views_plugin_argument_validate {
  var $option_name = 'validate_argument_privacypu';

  function validate_form(&$form, &$form_state) {
    $form[$this->option_name] = array(
      '#type' => 'select',
      '#title' => t('Privacy type'),
      '#options' => privacypu_get_types_options(),
      '#default_value' => $this->get_argument(),
      '#description' => t('Choose the privacy type for this view.  If you do not see the type that you need, you may add it via hook_privacypu_types_info().'),
    );
  }

  function validate_argument($argument) {
    return privacypu_check_access(user_load($argument), $this->argument->options[$this->option_name]);
  }
}